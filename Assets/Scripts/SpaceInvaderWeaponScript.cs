﻿using UnityEngine;
using System.Collections;

public class SpaceInvaderWeaponScript : MonoBehaviour {
	
	public static float speed;
	public static float damage;
	
	void Start()
	{
	}
	
	void Update()
	{	
		//if (transform.position.y > 6) {
		//	Destroy (this.gameObject);
		//}
		transform.position = transform.position - Vector3.up * speed * Time.deltaTime;
	}
	
	public static void UpdateWeaponVariables()
	{
		speed = StartScript.SIweaponSpeed;
		damage = StartScript.SIweaponDamage;
	}
	
}
