﻿using UnityEngine;
using System.Collections;

//using C# (C-sharp) but one can also use Javascriptor Utility's own syntax
public class SpaceInvaderController : MonoBehaviour {

	private static float speed;
	private static float drop; //drop of the invaders each left or right in y
	public float health;
	public bool goRight= true;
	private float maxX, minX; //boundaries plane
	public static int InvadersDestructed;
	private GameObject weapon; //player bullit
	public Canvas LoseMenu;
	public static Vector3 dropAmount;
	public static bool lose;

	public GameObject SIbullit; //SIweapon

	void Start ()
	{

		//LoseMenu = LoseMenu.GetComponent<Canvas> ();
		dropAmount = new Vector3 (0, -drop, 0);
		maxX = 5f;
		minX = -5f;
		InvadersDestructed = 0;

		lose = false; //gameObject SpaceInvader is alive
			//}
		//	spaceoffset = new Vector3 (-4.5f, 4.0f - row * 0.5f, -0.51f);
		//}

		InvokeRepeating("CreateBullit", 1.0f, 1.0f);
	}

	void Update ()
	{    
		//float maxX, minX;
		
//		maxX = 5f -offset;
//		minX = -5f +offset;
//		
		
		//ping pong the invader
		//transform.position = Vector2.Lerp (pos1, pos2, Mathf.PingPong(Time.time*speed, 1.0f));
		
		if (transform.position.x > maxX) {
			goRight = false;
			dropALevel();
		} 
		
		else if (transform.position.x < minX) {
			goRight = true;
			dropALevel();
		}
		
		move (goRight);

		//check health
		if (health <= 0) 
		{
			InvadersDestructed = InvadersDestructed + 1;
			Destroy(gameObject);
		}

		if (transform.position.y < -4.1) {
			lose = true;
		}

		if (lose == true) {
			//LoseMenu.enabled = true;
			Destroy (gameObject);
		}
		
	}
	
	void dropALevel() {

		transform.Translate (dropAmount);
	}
	
	void move(bool goRight) {
		Vector3 moveX = new Vector3 (speed * Time.deltaTime, 0, 0); 
		if (goRight) {
			transform.Translate(moveX);
		}
		else transform.Translate (-moveX);
	}

	//colliding with a weapon bullet
	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "weapon")
		{
		Destroy(col.gameObject); //destroy weapon
		weapon = col.gameObject;
		health = health - Weaponscript.damage; //health of the spaceinvader
		}
	}

	public static void UpdateSpaceInvaderVariables()
	{
		speed = StartScript.spaceInvaderSpeed;
		drop = StartScript.spaceInvaderDrop;
	}
	

	void CreateBullit() {
		Instantiate(SIbullit, transform.position, Quaternion.identity);
	}
	
}


