﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EndOfGameScript : MonoBehaviour {

	public Canvas quitMenu;
	public Button returnText;
	public Button quitText;

	public Text winText;
	public Text loseText;

	// Use this for initialization when loading the endScene
	void Start () {

		quitMenu = quitMenu.GetComponent<Canvas> ();
		returnText = returnText.GetComponent<Button> ();
		quitText = quitText.GetComponent<Button> ();
		quitMenu.enabled = false;

		winText = winText.GetComponent<Text> ();
		loseText = loseText.GetComponent<Text> ();

//		//Test
//		winText.enabled = true;
//		loseText.enabled = false;


		//check if won or not
		if (StartScript.win == true) {
			winText.enabled = true;
			loseText.enabled = false;
		} else {
			loseText.enabled = true;
			winText.enabled = false;
		}

	}
	

	public void returnPress () { //return to startscene
		Application.LoadLevel ("StartScene");
	
	}

	public void quitPress () { //press quit the game (button quitText)
		quitMenu.enabled = true;
		returnText.enabled = false;
		quitText.enabled = false;
		//maybe also disable winText or loseText?
		if (StartScript.win) {
			winText.enabled = false;
		} else {
			loseText.enabled= false;
		}
	}

	public void yesPress () { //press yes on quitmenu
		Application.Quit (); //quit the game
	}

	public void noPress () { //press no on the quitmenu, go back to endScene
		quitMenu.enabled = false;
		returnText.enabled = true;
		quitText.enabled = true;

		//enable winText or loseText
		if (StartScript.win) {
			winText.enabled = true;
		} else {
			loseText.enabled= true;
		}
	}

}
