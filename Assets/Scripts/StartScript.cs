﻿using UnityEngine;
using System.Collections;

public class StartScript : MonoBehaviour {//Startof the game, contains nost start() methods from gameObjects

	public GameObject SpaceInvader;
	public GameObject weapon;
	public GameObject shield;
	public GameObject player;
	//public Canvas LoseMenu;

	public static float weaponSpeed;
	public static float weaponDamage;
	public static float spaceInvaderSpeed;
	public static float spaceInvaderDrop;
	public static int rowsOffInvaders;
	public static int collumsOffInvaders;
	public static float playerSpeed;
	public static bool win;
	public static bool lose;

	public static float SIweaponSpeed;
	public static float SIweaponDamage;

	// Use this for initialization
	void Start () {

		weaponSpeed = 10;
		weaponDamage = 10;
		win = false;
		rowsOffInvaders = 3;
		collumsOffInvaders = 8;
		playerSpeed = 10;
		spaceInvaderSpeed = 1;
		spaceInvaderDrop = 1.5f;

		SIweaponSpeed = 10;
		SIweaponDamage = 10;

		UpdateVariables ();
		InstantiateSpaceInvaders ();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (SpaceInvaderController.InvadersDestructed == collumsOffInvaders*rowsOffInvaders)
		{	
			win = true;
			Application.LoadLevel(2); //load endScene
		}

	}

	public void InstantiateSpaceInvaders()
	{
		
		Vector3 spaceoffset = new Vector3(-4.5f,4.0f,-0.51f);
		
		for (int row = 0; row < rowsOffInvaders; row++)
		{
			for (int col = 0; col < collumsOffInvaders; col++) 
			{
				Instantiate (SpaceInvader, spaceoffset, Quaternion.identity);
				spaceoffset = spaceoffset + new Vector3 (1, 0, 0);
				
			}
			spaceoffset = new Vector3 (-4.5f, 3f - row, -0.51f);
		}
	}

	public void UpdateVariables()
	{
		SpaceInvaderController.UpdateSpaceInvaderVariables ();
		Playercontroller.updatePlayerVariables ();
		Weaponscript.UpdateWeaponVariables ();
	}

}
