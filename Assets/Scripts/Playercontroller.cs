﻿using UnityEngine;
using System.Collections;

public class Playercontroller : MonoBehaviour {

	private static float speed;
	public GameObject bullit;
	public static Vector3 offset = new Vector3(0,0.6f,0);

	private GameObject SIweapon;
	public float health;
	
	void Start ()
	{
	}
	
	
	void Update ()	
	{	//First with forces
//			float movement = Input.GetAxis ("Horizontal") * speed;
//			rb.AddForce (movement, 0, 0);

		//go right
		if (Input.GetKey (KeyCode.RightArrow))
		{ //use GetKeyDown if you want to have one moevement per press on the key
			float movement = Input.GetAxis ("Horizontal") * speed * Time.deltaTime;
			transform.position += Vector3.right * movement;
		}

		//go left
		if (Input.GetKey (KeyCode.LeftArrow))
		{ //use GetKeyDown if you want to have one moevement per press on the key
			float movement = Input.GetAxis ("Horizontal") * speed * Time.deltaTime;
			transform.position += Vector3.right * movement;
		}


		//maak bullit met spatiebalk
		if (Input.GetKeyDown("space"))
		{
			Instantiate(bullit,transform.position + offset,Quaternion.identity);
		}
	
	}

	public static void updatePlayerVariables()
	{
		speed = StartScript.playerSpeed;
	}

	void OnCollisionEnter(Collision col) //collide with SIweapon
	{
		if (col.gameObject.tag == "SIweapon") //spaceInvaderweapon
		{
			Destroy(col.gameObject); //destroy weapon
			SIweapon = col.gameObject;
			health = health - SpaceInvaderWeaponScript.damage; //health of the spaceinvader
		}
	}
	

	
}
