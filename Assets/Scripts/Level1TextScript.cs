﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Level1TextScript : MonoBehaviour { //

	public Button Back;
	private Canvas LoseMenu;

	void Start()
	{
		Back = Back.GetComponent<Button>();
		LoseMenu = this.GetComponent<Canvas> ();
		LoseMenu.enabled = false;
	}

	public void BackToMenu()
	{
		Application.LoadLevel (2);
	}

	public void onLosing()
	{
		LoseMenu.enabled = true;
	}
}
