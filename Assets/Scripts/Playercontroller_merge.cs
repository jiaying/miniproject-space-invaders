﻿using UnityEngine;
using System.Collections;

public class Playercontroller_merge : MonoBehaviour {

	public float speed;
	public GameObject spaceinvader;
	public GameObject bullit;
	public Rigidbody rb;
	private Vector3 offset;
	public int rowsOffInvaders;
	public int collumsOffInvaders;

	public static bool win; 
	
	void Start ()
	{	win = false;

		Vector3 spaceoffset = new Vector3(-4.5f,4.0f,-0.51f);
		for (int row = 0; row < rowsOffInvaders; row++)
		{
			for (int col = 0; col < collumsOffInvaders; col++) 
			{
		Instantiate (spaceinvader, spaceoffset, Quaternion.identity);
		spaceoffset = spaceoffset + new Vector3 (1, 0, 0);
			
			}
			spaceoffset = new Vector3 (-4.5f, 4.0f - row, -0.51f);
		}
		rb = GetComponent<Rigidbody> ();

		//the bullit is nog niet gemaakt
		bullit.gameObject.SetActive (true);
	}
	
	
	void Update ()	
	{	//First with forces
//			float movement = Input.GetAxis ("Horizontal") * speed;
//			rb.AddForce (movement, 0, 0);

		//go right
		if (Input.GetKey (KeyCode.RightArrow)) { //use GetKeyDown if you want to have one moevement per press on the key
			float movement = Input.GetAxis ("Horizontal") * speed * Time.deltaTime;
			transform.position += Vector3.right * movement;
		}

		//go left
		if (Input.GetKey (KeyCode.LeftArrow)) { //use GetKeyDown if you want to have one moevement per press on the key
			float movement = Input.GetAxis ("Horizontal") * speed * Time.deltaTime;
			transform.position += Vector3.right * movement;
		}
	
		//if (SpaceInvaderController.maxX < transform.position.x && SpaceInvaderController.minX > transform.position.x) {
		//	rb.velocity = Vector3.zero;
		//}

		//maak bullit met spatiebalk
		//if (Input.GetKeyDown ("space"))
		if(Input.GetKey(KeyCode.Space))
		{	
			offset.Set(0,0.6f,0);
			Instantiate(bullit,transform.position + offset,Quaternion.identity);
		}

		if (SpaceInvaderController.InvadersDestructed == collumsOffInvaders*rowsOffInvaders)
		{	win = true;
			Application.LoadLevel(2); //load endScene
		}
	}
	
}
